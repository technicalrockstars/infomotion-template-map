var ol = require("openlayers");

HeatMap.defaultSettings = {
	tooltipsToDisplay:[
	{value:"uu"},
	{value:"pv"},
	{value:"ss"}],
	longitude:"longitude",
	latitude:"latitude",
	value:"os_flow"
};

HeatMap.settings = EnebularIntelligence.SchemaProcessor([
{
  type : "key", name : "longitude", help : ""
},{
  type : "key", name : "latitude", help : ""
},
{
  type : "key", name : "value", help : ""
},
{
  type : "list", name : "tooltipsToDisplay", help : "", children:[
	{
  		type : "key", name : "value", help : ""
	}
  ]
}
], HeatMap.defaultSettings);



function HeatMap(settings, options) {
    var that = this;
    this.el = window.document.createElement('div');
    this.settings = settings;
    this.options = options;
    this.data = [];
	this.map_rendered = false;

    this.chartWidth = 700;
    this.chartHeight = 500;


	this.base = d3.select(this.el)
		.append('div')
		.classed('control-base',true)

	this.tooltip = d3.select(this.el)
		.append('div')
		.classed('tooltip',true)
		.classed('hidden', true)

	this.mapbase = d3.select(this.el)
		.append('div')
		.attr('id', 'map')
		.classed('map',true)

	this.formatTime = d3.time.format("%B %d, %Y");

	this.valueToUse = this.settings.value;
	this.long = this.settings.longitude;
	this.lat = this.settings.latitude;
	this.tooltipsToDisplay = this.settings.tooltipsToDisplay;

}




HeatMap.prototype.refresh = function() {
    var that = this;

	if(!this.center)
		this.center = that.map.getView().getCenter();

	var features = [];
	
	this.data.forEach(function(d){ 

		var dx = d[that.lat], 
			dy = d[that.long]; 
		
		var coord = ol.proj.transform([dx,dy], 'EPSG:4326', 'EPSG:3857'); 
		
		if(isNaN(coord[0]) || isNaN(coord[1])){ 
			return; 
		} 

		feature = new ol.Feature({
			geometry: new ol.geom.Point([coord[0],coord[1]]),
			radius: 5,
			data : d,
			tooltip_text : that.toolTipText(d),
		});
		
		console.log(feature)

		features.push(feature);
	})

	var clusterSource = new ol.source.Cluster({
		distance: 20,
		source: new ol.source.Vector({
			features: features
		})
	});

	if(!this.heatmap) {
		this.heatmap = this.initHeatMap(clusterSource);
		console.log('HeatMap Init');
		this.map.addLayer( this.heatmap );
		console.log('post Init');
	}else{
		this.heatmap.setSource ( clusterSource );
		console.log('HeatMap Update');
	}

	this.map.render();


}

HeatMap.prototype.toolTipText = function(d){
	var textToDisplay = '';
	textToDisplay += 'Created: ' + this.formatTime(new Date(d.created));
	this.tooltipsToDisplay.forEach(function(eachToolTip){
		if(d.hasOwnProperty(eachToolTip.value)){
			textToDisplay += ', '+ eachToolTip.value+': '+d[eachToolTip.value];
		}
	})
	return textToDisplay;
}


HeatMap.prototype.initMap = function() {
	that = this;
	console.log('init')
	var centerPos = ol.proj.transform([135.330032,33.6750984], 'EPSG:4326', 'EPSG:3857');

    this.map = new ol.Map({
          target: 'map',
          layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()

            })
          ],
          view: new ol.View({
            center: centerPos,
            zoom: 4
          })
        });


    var displayFeatureInfo = function(pos) {

        var f = that.map.forEachFeatureAtPixel(pos, function(feature) {
          return feature;
        });

        if (f) {
			var features = f.get('features'), text = [];
			console.log(features)
			
			features.forEach(function(d) {
				text.push( d.get('tooltip_text') );
			})

			that.tooltip
				.html( text.join("<br>") )
				.style("left", (pos[0]+15) + "px")
				.style("top", (pos[1] +5) + "px")
				.classed('hidden',false)
        } else {
			that.tooltip
				.classed('hidden', true);
        }
      };

	this.map.on('pointermove', function(e) {
        if (e.dragging) {
			that.tooltip
				.classed('hidden', true);
			return;
        }
        displayFeatureInfo(that.map.getEventPixel(e.originalEvent));
      });

     return true;
 }

HeatMap.prototype.initHeatMap = function(src) {
   var that = this;

   return new ol.layer.Heatmap({
	   opacity: 0.9,
        source: src,
        blur: 10,
        radius: 15,
        weight:function(srcObject) {
        	console.log("here")
          var features = srcObject.get('features'),
				size = features.length,
				sum = 0, count = 0, data;
          for( var i = 0; i < size; i++ )
            if (data = features[i].get('data')) {
              sum += data[that.valueToUse];
              count++;
            }
          var avg = count ? sum / count : 0;
          return avg/10000;
        }
  });

}

HeatMap.prototype.resize = function(options) {

    if(!this.map_rendered) 	this.map_rendered = this.initMap();

}

HeatMap.prototype.addData = function(data) {

    var that = this;
    if(data instanceof Array) {
        data.forEach(function(d) {
            that.data.push(d);
        });
        this.liveMode = false;
        this.refresh();
       // console.log(data[0]);

    }else{
		if(!this.liveMode) {
			this.data = [];
		}
        this.data.push(data);
        this.liveMode = true;
        this.refresh();
    }
}

HeatMap.prototype.getEl = function() {
  return this.el;
}

HeatMap.prototype.clearData = function() {
    this.data = [];
    this.refresh( );
}

window.EnebularIntelligence.register('HeatMap', HeatMap);

module.exports = HeatMap;